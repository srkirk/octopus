!! Copyright (C) 2002-2006 M. Marques, A. Castro, A. Rubio, G. Bertsch
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module handles groups of electronic batches and their parallel distribution

module states_elec_group_oct_m
  use batch_oct_m
  use debug_oct_m
  use global_oct_m
  use messages_oct_m
  use profiling_oct_m
  use states_elec_dim_oct_m
  use wfs_elec_oct_m

  implicit none

  private

  public ::                           &
    states_elec_group_t,                   &
    states_elec_group_end,                 &
    states_elec_group_copy

  !> @brief Group of electronic states
  !!
  !! This class represents a group of batches of wave functions. It contains
  !! groups of blocks (batches) electronic states, represented by wfs_elec_oct_m::wfs_elec_t,
  !! together with the meta-data how these blocks are organized.
  !!
  !! Each block contains wave functions with the same k point.
  !! The blocks can be distributed over nodes, when using state parallelization.
  !
  type states_elec_group_t
    ! Components are public by default
    type(wfs_elec_t), allocatable :: psib(:, :)             !< An array of wave-functions blocks (batches)
    integer                  :: nblocks                     !< The total number of blocks (across nodes)
    integer                  :: block_start                 !< The lowest index of local blocks
    integer                  :: block_end                   !< The highest index of local blocks
    integer, allocatable     :: iblock(:)                   !< A map, that for each state index returns the index of block containing it
    integer, allocatable     :: block_range(:, :)           !< Each block contains states from block_range(:, 1) to block_range(:, 2)
    integer, allocatable     :: block_size(:)               !< The number of states in each block.
    logical, allocatable     :: block_is_local(:, :)        !< It is true if the block (ist, ik) is in this node.
    integer, allocatable     :: block_node(:)               !< The node that contains each block
    integer, allocatable     :: rma_win(:, :)               !< The MPI window handle for for block (ist, ik)
    !!                                                         for one sided communication
    logical                  :: block_initialized = .false. !< For keeping track of the blocks to avoid memory leaks
  end type states_elec_group_t

contains

  ! ---------------------------------------------------------
  !> @brief finalize the local blocks of wave functions and release local arrays
  !
  subroutine states_elec_group_end(this, d)
    type(states_elec_group_t), intent(inout) :: this  !< the group to end
    type(states_elec_dim_t),   intent(in)    :: d     !< dimensions of the wave functions (spin and k-points)

    integer :: ib, iq

    PUSH_SUB(states_elec_group_end)

    if (this%block_initialized) then
      do ib = 1, this%nblocks
        do iq = d%kpt%start, d%kpt%end
          if (this%block_is_local(ib, iq)) then
            call this%psib(ib, iq)%end()
          end if
        end do
      end do

      SAFE_DEALLOCATE_A(this%psib)

      SAFE_DEALLOCATE_A(this%iblock)
      SAFE_DEALLOCATE_A(this%block_range)
      SAFE_DEALLOCATE_A(this%block_size)
      SAFE_DEALLOCATE_A(this%block_is_local)
      SAFE_DEALLOCATE_A(this%block_node)
      this%block_initialized = .false.
    end if

    POP_SUB(states_elec_group_end)
  end subroutine states_elec_group_end

  !---------------------------------------------------------
  !> @brief make a copy of a group
  !
  subroutine states_elec_group_copy(d, group_in, group_out, copy_data, special)
    type(states_elec_dim_t),   intent(in)    :: d         !< dimensions of the wave functions (spin and k-points)
    type(states_elec_group_t), intent(in)    :: group_in  !< original group
    type(states_elec_group_t), intent(inout) :: group_out !< destination group
    logical, optional,         intent(in)    :: copy_data !< optional flag whether to perform a deep copy; default=.true.
    logical, optional,         intent(in)    :: special   !< allocate on GPU if possible

    integer :: qn_start, qn_end, ib, iqn

    PUSH_SUB(states_elec_group_copy)

    call states_elec_group_end(group_out, d)

    group_out%nblocks           = group_in%nblocks
    group_out%block_start       = group_in%block_start
    group_out%block_end         = group_in%block_end
    group_out%block_initialized = group_in%block_initialized

    if (group_out%block_initialized) then

      ASSERT(allocated(group_in%psib))

      qn_start = d%kpt%start
      qn_end   = d%kpt%end

      SAFE_ALLOCATE(group_out%psib(1:group_out%nblocks, qn_start:qn_end))

      do iqn = qn_start, qn_end
        do ib = group_out%block_start, group_out%block_end
          call group_in%psib(ib, iqn)%copy_to(group_out%psib(ib, iqn), &
            copy_data = optional_default(copy_data, .true.), special = special)
        end do
      end do

      SAFE_ALLOCATE_SOURCE_A(group_out%iblock, group_in%iblock)
      SAFE_ALLOCATE_SOURCE_A(group_out%block_range, group_in%block_range)
      SAFE_ALLOCATE_SOURCE_A(group_out%block_size, group_in%block_size)
      SAFE_ALLOCATE_SOURCE_A(group_out%block_is_local, group_in%block_is_local)
      SAFE_ALLOCATE_SOURCE_A(group_out%block_node, group_in%block_node)
      SAFE_ALLOCATE_SOURCE_A(group_out%rma_win, group_in%rma_win)

    end if

    POP_SUB(states_elec_group_copy)
  end subroutine states_elec_group_copy

end module states_elec_group_oct_m


!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
