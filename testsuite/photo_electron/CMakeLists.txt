foreach (test IN ITEMS
		01-h1d_lin
		02-restart
		03-h1d_ati
		04-nfft
		05-pfft
		06-2D
		07-flux_1d
		09-flux_3d
		10-spm_1d
		12-spm_3d
		13-arpes_2d
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
