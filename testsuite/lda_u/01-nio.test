# -*- coding: utf-8 mode: shell-script -*-

Test       : GGA+U for (AF) bulk NiO crystal
Program    : octopus
TestGroups : long-run, periodic_systems, lda_u
Enabled    : Yes

Input      : 01-nio.01-U5-gs.inp
match ;  Total k-points    ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 8.0
match ;  Reduced k-points  ; GREPFIELD(static/info, 'Number of symmetry-reduced k-points', 6) ; 4.0

Precision: 1.11e-05
match ;   Total energy              ; GREPFIELD(static/info, 'Total       =', 3) ; -247.1481281
Precision: 8.85e-08
match ;   Ion-ion energy            ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -177.00987193
Precision: 7.89e-06
match ;   Hartree energy            ; GREPFIELD(static/info, 'Hartree     =', 3) ; 46.417766900000004
Precision: 1.39e-06
match ;   Exchange energy           ; GREPFIELD(static/info, 'Exchange    =', 3) ; -27.75870298
Precision: 1.02e-07
match ;   Correlation energy        ; GREPFIELD(static/info, 'Correlation =', 3) ; -2.03658382
Precision: 1.16e-05
match ;   External energy           ; GREPFIELD(static/info, 'External    =', 3) ; -210.71345142
Precision: 7.04e-06
match ;   Eigenvalues sum           ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -33.52500638
Precision: 4.44e-06
match ;   Kinetic energy            ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 123.74311296
Precision: 1.06e-05
match ;   Hubbard energy            ; GREPFIELD(static/info, 'Hubbard     =', 3) ; 0.212017

Precision: 3.00e-05
match ;   Total Magnetic Moment   ; GREPFIELD(static/info, 'mz = ', 3) ; 6e-06
Precision: 1.67e-05
match ;   Local Magnetic Moment (Ni1)   ; GREPFIELD(static/info, '1        Ni', 3) ; 3.349431
match ;   Local Magnetic Moment (Ni2)   ; GREPFIELD(static/info, '2        Ni', 3) ; -3.34943
Precision: 2.00e-06
match ;   Local Magnetic Moment (O1)   ; GREPFIELD(static/info, '3         O', 3) ; 0.0
match ;   Local Magnetic Moment (O2)   ; GREPFIELD(static/info, '4         O', 3) ; 0.0

Precision: 4.36e-06
match ;    Occupation Ni2 down 3d4    ; LINEFIELD(static/occ_matrices, -2, 7) ; 0.8725035
Precision: 1.03e-06
match ;    Occupation Ni2 down 3d5    ; LINEFIELD(static/occ_matrices, -1, 9) ; 0.88857711

Precision: 1.00e-04
match ;    k-point 1 (x)    ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
match ;    k-point 1 (y)    ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
match ;    k-point 1 (z)    ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.55e-05
match ;    Eigenvalue  1    ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -3.0909959999999996
Precision: 1.48e-05
match ;    Eigenvalue  8    ; GREPFIELD(static/info, '#k =       1', 3, 8) ; -2.961122
Precision: 2.20e-04
match ;    Eigenvalue 16    ; GREPFIELD(static/info, '#k =       1', 3, 16) ; -0.43904
Precision: 8.31e-06
match ;    Eigenvalue 17    ; GREPFIELD(static/info, '#k =       1', 3, 17) ; -0.166105

Input      : 01-nio.02-unocc.inp

Precision: 1.00e-04
match ;   Red. coord. k1   ; LINEFIELD(static/bandstructure-sp1, 2, 1) ; 0.0
Precision: 1.00e-04
match ;   Band structure k1x   ; LINEFIELD(static/bandstructure-sp1, 2, 2) ; 0.0
match ;   Band structure k1y   ; LINEFIELD(static/bandstructure-sp1, 2, 3) ; 0.0
match ;   Band structure k1z   ; LINEFIELD(static/bandstructure-sp1, 2, 4) ; 0.0
Precision: 1e-6
match ;      Band structure E1(k1)      ; LINEFIELD(static/bandstructure-sp1, 2, 5) ; -3.0909946799999997
Precision: 1e-6
match ;      Band structure E2(k1)      ; LINEFIELD(static/bandstructure-sp1, 2, 6) ; -3.08367678
Precision: 5.50e-07
match ;      Band structure E3(k1)      ; LINEFIELD(static/bandstructure-sp1, 2, 7) ; -3.0772917300000002

Precision: 2.50e+00
match ;   Red. coord. k3   ; LINEFIELD(static/bandstructure-sp1, 4, 1) ; 0.5
Precision: 1.01e-06
match ;   Band structure k3x   ; LINEFIELD(static/bandstructure-sp1, 4, 2) ; -0.01562
