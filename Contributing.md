# Contributing

See [official website page](https://octopus-code.org/documentation/main/developers/).

## Adding Modules to CMakeLists.txt

A `CMakeLists.txt` file is defined in each subfolder of the `src/`. In order to add a new fortran source file to
the build, simply append the `target_sources` arguments with the file name:

```cmake
target_sources(Octopus_lib PRIVATE
		module.F90
		...
)
```

This specifies sources to use when building a target `Octopus_lib`.
