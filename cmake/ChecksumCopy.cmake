# Script to be called by the target in `oct_add_checksum_copy` in `helpers.cmake`
# Check the checksums of all the files in `checksum_src` and copy the files if they differ from `checksum_dest`

# Variables::
#   checksum_src (path): location of the source to copy from
#   checksum_dest (path): location of the source to copy to

# Check if directory does not exist
if (NOT EXISTS ${checksum_dest})
	# If doesn't exist can just copy everything
	file(COPY ${checksum_src}/ ${checksum_dest})
	return()
endif ()

# Check each file
file(GLOB_RECURSE src_files RELATIVE ${checksum_src} ${checksum_src}/*)
foreach (file IN LISTS src_files)
	if (CMAKE_VERSION VERSION_GREATER_EQUAL 3.21)
		file(COPY_FILE ${checksum_src}/${file} ${checksum_dest}/${file}
				ONLY_IF_DIFFERENT)
	else ()
		# Manually check content hash
		file(MD5 ${checksum_src}/${file} _old_hash)
		# Set a fake hash to be used when file does not yet exist
		set(_new_hash "fake_hash")
		if (EXISTS ${checksum_dest}/${file})
			file(MD5 ${checksum_dest}/${file} _new_hash)
		endif ()
		if (NOT _old_hash STREQUAL _new_hash)
			configure_file(${checksum_src}/${file} ${checksum_dest}/${file} COPYONLY)
		endif ()
	endif ()
endforeach ()
