set output 'tutorial_03.1-plot_e_field_at_orig.png
set term png size 500,400
set xlabel 'time step'
set ylabel 'Total E field E_z'
p 'Maxwell/td.general/total_e_field_z' u 1:3 w l

