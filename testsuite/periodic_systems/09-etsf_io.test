# -*- coding: utf-8 mode: shell-script -*-

Test       : ETSF_IO
Program    : octopus
TestGroups : long-run, periodic_systems
Enabled    : no-GPU

Input      : 09-etsf_io.01-gs.inp

match ; SCF convergence ; GREPCOUNT(static/info, 'SCF converged') ; 1

match ; Total k-points   ; GREPFIELD(static/info, 'Total number of k-points', 6) ; 2

Precision: 1.57e-07
match ;   Total energy         ; GREPFIELD(static/info, 'Total       =', 3) ; -31.42693262
Precision: 1.57e-06
match ;   Ion-ion energy       ; GREPFIELD(static/info, 'Ion-ion     =', 3) ; -31.4312028
Precision: 2.38e-07
match ;   Eigenvalues sum      ; GREPFIELD(static/info, 'Eigenvalues =', 3) ; -0.47681028
Precision: 1.24e-08
match ;   Hartree energy       ; GREPFIELD(static/info, 'Hartree     =', 3) ; 2.484390735
Precision: 5.50e-09
match ;   Exchange energy      ; GREPFIELD(static/info, 'Exchange    =', 3) ; -8.244417635
Precision: 7.54e-04
match ;   Correlation energy   ; GREPFIELD(static/info, 'Correlation =', 3) ; -1.5076999999999998
Precision: 6.61e-08
match ;   Kinetic energy       ; GREPFIELD(static/info, 'Kinetic     =', 3) ; 13.22897847
Precision: 2.98e-06
match ;   External energy      ; GREPFIELD(static/info, 'External    =', 3) ; -5.956981300000001
Precision: 1.00e-04
match ;   k-point 1 (x)   ; GREPFIELD(static/info, '#k =       1', 7) ; 0.0
Precision: 1.00e-04
match ;   k-point 1 (y)   ; GREPFIELD(static/info, '#k =       1', 8) ; 0.0
Precision: 1.00e-04
match ;   k-point 1 (z)   ; GREPFIELD(static/info, '#k =       1', 9) ; 0.0
Precision: 1.40e-05
match ;   Eigenvalue  1   ; GREPFIELD(static/info, '#k =       1', 3, 1) ; -0.280389
Precision: 2.65e-05
match ;   Eigenvalue  8   ; GREPFIELD(static/info, '#k =       1', 3, 8) ; 0.05291200000000001
Precision: 8.03e-05
match ;   Eigenvalue 16   ; GREPFIELD(static/info, '#k =       1', 3, 16) ; 0.16066
Precision: 8.26e-06
match ;   Eigenvalue 18   ; GREPFIELD(static/info, '#k =       1', 3, 18) ; 0.165269
Precision: 2.50e+00
match ;   k-point 2 (x)   ; GREPFIELD(static/info, '#k =       2', 7) ; 0.5
Precision: 2.50e+00
match ;   k-point 2 (y)   ; GREPFIELD(static/info, '#k =       2', 8) ; 0.5
Precision: 2.50e+00
match ;   k-point 2 (z)   ; GREPFIELD(static/info, '#k =       2', 9) ; 0.5
Precision: 9.59e-06
match ;   Eigenvalue  1   ; GREPFIELD(static/info, '#k =       2', 3, 1) ; -0.191874
Precision: 4.95e-05
match ;   Eigenvalue  8   ; GREPFIELD(static/info, '#k =       2', 3, 8) ; -0.098903
Precision: 5.76e-06
match ;   Eigenvalue 16   ; GREPFIELD(static/info, '#k =       2', 3, 16) ; 0.115181
Precision: 1.09e-04
match ;   Eigenvalue 18   ; GREPFIELD(static/info, '#k =       2', 3, 18) ; 0.21725
# ETSF_IO files are binary, so we cannot easily test the contents, but
# at least we can check that this run finished without crashing,
# and test the size. However, these sizes are correct only for ETSF_IO 1.0.3/1.0.4;
# ETSF_IO 1.0.2 will give ones 208 smaller.
# Note that the sizes unfortunately depend on the length of the PACKAGE_STRING for
# the current version of octopus, and must be updated when that changes.

if (available netcdf); then
  if (available etsf_io); then
## Disabled bit size checking. It is dependent on PACKAGE_STRING which is varying in autotools/cmake, between versions
## etc.
  else
      match; Error missing ETSF_IO ; GREPCOUNT(err, 'Octopus was compiled without ETSF_IO support') ; 1
  endif
else
  match; Error missing NETCDF ; GREPCOUNT(err, 'Octopus was compiled without NetCDF support') ; 1
endif
