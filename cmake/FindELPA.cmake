#[==============================================================================================[
#                                  ELPA compatibility wrapper                                  #
]==============================================================================================]

#[===[.md
# FindELPA

ELPA compatibility module for Octopus

This file is specifically tuned for Octopus usage.

]===]

list(APPEND CMAKE_MESSAGE_CONTEXT FindELPA)
include(Octopus)

set(_find_elpa True)
set(_elpa_fcflags)
set(_required_arg)
if (OpenMP IN_LIST ${CMAKE_FIND_PACKAGE_NAME}_FIND_COMPONENTS)
	if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED_OpenMP)
		list(APPEND _required_arg REQUIRED)
	endif ()
	pkg_check_modules(${CMAKE_FIND_PACKAGE_NAME}
			${_required_arg}
			${${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY}
			IMPORTED_TARGET
			elpa_openmp)
	if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
		pkg_get_variable(_elpa_fcflags elpa_openmp fcflags)
		add_library(ELPA::OpenMP ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
		set(_find_elpa False)
	endif ()
endif ()
if (_find_elpa)
	if (${CMAKE_FIND_PACKAGE_NAME}_FIND_REQUIRED)
		list(APPEND _required_arg REQUIRED)
	endif ()
	pkg_check_modules(${CMAKE_FIND_PACKAGE_NAME}
			${_required_arg}
			${${CMAKE_FIND_PACKAGE_NAME}_FIND_QUIETLY}
			IMPORTED_TARGET
			elpa)
	pkg_get_variable(_elpa_fcflags elpa fcflags)
endif ()
if (TARGET PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
	add_library(ELPA::elpa ALIAS PkgConfig::${CMAKE_FIND_PACKAGE_NAME})
	if (_elpa_fcflags)
		target_compile_options(PkgConfig::${CMAKE_FIND_PACKAGE_NAME} INTERFACE
				$<$<COMPILE_LANGUAGE:Fortran>:${_elpa_fcflags}>)
	endif ()
endif ()
if (${CMAKE_FIND_PACKAGE_NAME}_FOUND)
	set(HAVE_ELPA 1)
endif ()
set_package_properties(${CMAKE_FIND_PACKAGE_NAME} PROPERTIES
		URL https://gitlab.mpcdf.mpg.de/elpa/elpa
		DESCRIPTION "Eigenvalue Solvers for Petaflop Apllications (ELPA)"
)
list(POP_BACK CMAKE_MESSAGE_CONTEXT)
