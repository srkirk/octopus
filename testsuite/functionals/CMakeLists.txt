foreach (test IN ITEMS
		01-xc_1d
		02-xc_2d
		03-xc
		04-oep
		05-ks_inversion
		06-rdmft
		07-sic
		08-vdw_ts
		09-vdw_ts_sc
		10-vdw_d3_dna
		11-vdw_d3
		12-vdw_solid_c6
		13-libvdwxc_h2o
		14-libvdwxc_Be_hcp
		15-oep-CG
		16-dressed-rdmft
		17-oep-photons
		18-mgga
)
	Octopus_add_test(${test}
			TEST_NAME ${curr_path}/${test}
	)
endforeach ()
