#/bin/bash

module load gsl
module load lapack
module load blacs
module load scalapack

export PFFT_HOME="$HOME/local/pfft-threads-1.0.5-alpha"
export FFTW3_HOME="$HOME/local/fftw-threads-3.3.2"
export LD_LIBRARY_PATH=$PFFT_HOME/lib:$FFTW3_HOME/lib:$LD_LIBRARY_PATH

export FC_INTEGER_SIZE=8
export CC_FORTRAN_INT=int
export CC=mpicc
export CFLAGS="-g -Wl,-relax -O3 -I$PFFT_HOME/include -I$FFTW3_HOME/include"
export FC=mpif90
export FCFLAGS=$CFLAGS" -qxlf90=autodealloc -qessl -qsmp=omp "
export LIBS_BLAS="-lesslsmpbg -L/opt/ibmmath/essl/4.4/lib -lesslbg"
export LIBS_LAPACK="-L$LAPACK_LIB -llapack"

export LIBS_FFT="-L$FFTW3_HOME/lib/ -lfftw3_mpi -lfftw3 -lm"
echo "GSL DIR = $GSL_HOME "
echo "PFFT DIR = $PFFT_HOME "
export LDFLAGS=$LDFLAGS" -R/opt/ibmcmp/lib/bg/bglib \
-L/bgsys/drivers/V1R4M2_200_2010-100508P/ppc/gnu-linux/powerpc-bgp-linux/lib \
-L/bgsys/drivers/V1R4M2_200_2010-100508P/ppc/gnu-linux/lib/gcc/powerpc-bgp-linux/4.1.2 \
-L/opt/ibmcmp/xlf/bg/11.1/bglib \
-L/opt/ibmcmp/xlmass/bg/4.4/bglib \
-L/opt/ibmcmp/xlsmp/bg/1.7/bglib \
-L/bgsys/drivers/V1R4M2_200_2010-100508P/ppc/runtime/SPI \
-L/bgsys/drivers/V1R4M2_200_2010-100508P/ppc/comm/sys/lib \
-L/bgsys/drivers/V1R4M2_200_2010-100508P/ppc/comm/default/lib \
-L/usr/local/zlib/v1.2.3/lib \
-L/bgsys/drivers/ppcfloor/runtime/SPI \
-L$PFFT_HOME/lib -L$FFTW3_HOME/lib \
-lpfft -lfftw3_mpi -lfftw3 -lm \
-ldl -lxl -lxlopt -lrt -lpthread"

SVN_VERS=$(svn info .. | grep Revision | awk '{print $2}')
echo $SVN_VERS

../configure --prefix=$HOME/software/octopus_omp \
--with-libxc-prefix=$HOME/software/libxc_new \
--with-fft-lib="$FFTW3_HOME/lib/libfftw3.a  $FFTW3_HOME/lib/libfftw3_omp.a" \
--host=powerpc32-unknown-linux-gnu \
--build=powerpc64-unknown-linux-gnu \
--disable-gdlib \
--disable-f90-forall \
--with-blas="-L/opt/ibmmath/lib64 -lesslbg" \
--with-lapack="-L$LAPACK_LIB -llapack" \
--with-gsl-prefix=$GSL_HOME \
--enable-mpi --enable-openmp
