# #639 TODO: When removing autotools, replace flags with target_compile_define

list(APPEND CMAKE_MESSAGE_CONTEXT mock_autotools)
string(TIMESTAMP BUILD_TIME)
execute_process(
		COMMAND git log -1 --format=%h
		WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
		OUTPUT_VARIABLE GIT_HASH
		OUTPUT_STRIP_TRAILING_WHITESPACE
)

if (CMAKE_BUILD_TYPE MATCHES Debug.*)
	set(SHARE_DIR ${PROJECT_BINARY_DIR}/share)
else ()
	set(SHARE_DIR ${CMAKE_INSTALL_FULL_DATAROOTDIR}/octopus)
endif ()

include(CheckTypeSize)
check_type_size(size_t SIZEOF_SIZE_T LANGUAGE C)
check_type_size("unsigned int" SIZEOF_UNSIGNED_INT LANGUAGE C)
check_type_size("unsigned long" SIZEOF_UNSIGNED_LONG LANGUAGE C)
check_type_size("unsigned long long" SIZEOF_UNSIGNED_LONG_LONG LANGUAGE C)
check_type_size(uint32_t HAVE_UINT32_T LANGUAGE C)
check_type_size(uint64_t HAVE_UINT64_T LANGUAGE C)
include(cmake/CheckFortranTypeSizes.cmake)
check_fortran_type_sizes()
include(CheckIncludeFile)
include(CheckFunctionExists)
check_include_file(dirent.h HAVE_DIRENT_H)
check_include_file(errno.h HAVE_ERRNO_H)
check_include_file(signal.h HAVE_SIGNAL_H)
check_include_file(stdint.h HAVE_STDINT_H)
check_include_file(string.h HAVE_STRING_H)
check_include_file(windows.h HAVE_WINDOWS_H)
#check_include_file(iotcl.h HAVE_IOCTL)
# #642 TODO: Eliminate unnecessary function checks in code
#  NOTE: Some files do not have checks
check_function_exists(gettimeofday HAVE_GETTIMEOFDAY)
check_function_exists(nanosleep HAVE_NANOSLEEP)
check_function_exists(closedir HAVE_CLOSEDIR)
check_function_exists(readdir HAVE_READDIR)
check_function_exists(strchr HAVE_STRCHR)
check_function_exists(strtod HAVE_STRTOD)
check_function_exists(alphasort HAVE_ALPHASORT)
check_function_exists(scandir HAVE_SCANDIR)
check_function_exists(getopt_long HAVE_GETOPT_LONG)
check_function_exists(getpid HAVE_GETPID)
check_function_exists(perror HAVE_PERROR)
check_function_exists(sbrk HAVE_SBRK)
check_function_exists(sigaction HAVE_SIGACTION)
check_function_exists(strcasestr HAVE_STRCASESTR)
check_function_exists(strndup HAVE_STRNDUP)
check_function_exists(strsignal HAVE_STRSIGNAL)
check_function_exists(sysconf HAVE_SYSCONF)
check_function_exists(tcgetpgrp HAVE_TCGETPGRP)
check_function_exists(uname HAVE_UNAME)

if (MPI_Fortran_FOUND)
	set(MPI_MOD 1)
	set(HAVE_MPI 1)
endif ()
if (TARGET FFTW::Double OR TARGET MKL::MKL)
	set(HAVE_FFTW3 1)
	if (OCTOPUS_OpenMP AND (TARGET FFTW::DoubleOpenMP OR TARGET FFTW::DoubleThreads)
			OR (TARGET MKL::MKL AND MKL_THREADING AND NOT MKL_THREADING STREQUAL sequential))
		set(HAVE_FFTW3_THREADS 1)
	endif ()
endif ()
if (OCTOPUS_ScaLAPACK)
	set(HAVE_SCALAPACK 1)
endif ()

# Currently long lines have to be supported to avoid preprocess.pl
if (CMAKE_Fortran_COMPILER_ID MATCHES GNU)
	set(CMAKE_Fortran_FLAGS "${CMAKE_Fortran_FLAGS} -ffree-line-length-none")
endif ()
set(LONG_LINES 1)

try_compile(FC_Test_SUCCESS ${CMAKE_CURRENT_BINARY_DIR}
		SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/cmake/test_compiler.F90
		OUTPUT_VARIABLE FC_Test_OUTPUT)
if (NOT FC_Test_SUCCESS)
	message(SEND_ERROR "Failed to build test fortran file cmake/test_compiler.F90 with:
	CMAKE_Fortran_COMPILER: ${CMAKE_Fortran_COMPILER}
	CMAKE_Fortran_FLAGS: ${CMAKE_Fortran_FLAGS}")
	message(FATAL_ERROR ${FC_Test_OUTPUT})
endif ()
list(POP_BACK CMAKE_MESSAGE_CONTEXT)