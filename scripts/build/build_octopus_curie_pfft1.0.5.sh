#!/bin/bash

module load c/intel
module load fortran/intel
module load bullxmpi
module load mkl
module load gsl/1.14

WPREFIX=$WORKDIR/software
PREFIX=$HOME/software
export ISF_HOME="$WORKDIR/software/libisf"

export CC=mpicc
export CFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$ISF_HOME/include"
export FC=mpif90
export FCFLAGS="-xHost -O3 -m64 -ip -prec-div -static-intel -sox -I$PREFIX/fftw/3.3/include -I$ISF_HOME/include"

export LIBS_BLAS="${MKL_LIBS}"
export LIBS_FFT="-Wl,--start-group -L$PREFIX/pfft/1.0.5/lib -L$PREFIX/fftw/3.3/lib -lpfft -lfftw3 -lfftw3_mpi -Wl,--end-group"
export LDFLAGS=$LDFLAGS" -L$ISF_HOME/lib -lisfsolver -lrt"

../configure --prefix=$WPREFIX/octopus/superciliosus_pfft \
--disable-openmp --with-libxc-prefix=$WPREFIX/libxc/2.0.x \
--disable-gdlib --with-gsl-prefix=/usr/local/gsl-1.14 \
--enable-mpi --enable-newuoa \
--with-pfft-prefix=$PREFIX/pfft/1.0.5
