!! Copyright (C)  2020 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!> @brief This module implements the basic elements defining algorithms
!!
!! An algorithm is composed of algorithmic operations that are repeated in a loop.
!!
!! Unsurprisingly, those repetitions are called iterations.
!! The way how one keeps track of the iterations is with iteration counters.
!!
!! Then each iteration is divided into algorithmic steps,
!! and there is one interaction update per algorithmic step.
!! The concept of algorithmic step is necessary to ensure that the couplings are
!! updated at the right iteration, knowing that some systems might execute algorithms with
!! a different number of algorithmic steps and/or have a different granularity.
!!
! The following pseudocode shows how multiple algorithms, one per system, are executed:
! @code
!  repeat
!    for all systems do
!      algo_op ← next algorithmic operation
!      break ← f alse
!      while not break do
!        if algo_op != update interactions then
!          execute algorithmic operation
!          algo_op ← next algorithmic operation
!        else
!          try updating interactions
!          if interactions updated then
!            algo_op ← next algorithmic operation
!          end if
!          break ← true
!        end if
!      end while
!    end for
!  until all algorithms finished
! @endcode
!
! Note: The above codeblock is not enabled for doxygen, as the formatting is broken in doxygen.
!
!TODO: the concept of granularity also needs to be explained more in detail.
module algorithm_oct_m
  use debug_oct_m
  use global_oct_m
  use iteration_counter_oct_m
  use linked_list_oct_m
  use loct_oct_m
  use profiling_oct_m

  implicit none

  private
  public ::                  &
    algorithmic_operation_t, &
    algorithm_t,             &
    algorithm_iterator_t

  integer, parameter, public :: ALGO_LABEL_LEN = 50

  !> @brief Descriptor of one algorithmic operation.
  !!
  !! Algorithms are a sequence of operations. Those operations are identified by a
  !! string identifier that can be used in select case statements.
  type :: algorithmic_operation_t
    character(len=ALGO_LABEL_LEN) :: id    !< Operation identifier. We use a string instead of
    !!                                        an integer to minimize the chance of having duplicated identifiers.
    character(len=ALGO_LABEL_LEN) :: label !< Label describing what the code is doing when performing this operation.
  end type algorithmic_operation_t

  !# doc_start generic_algorithmic_operations
  !> Operations that can be used by any algorithm and, therefore, should be
  !! implemented by all systems.
  character(len=ALGO_LABEL_LEN), public, parameter ::   &
    SKIP                 = 'SKIP',                      &
    UPDATE_COUPLINGS     = 'UPDATE_COUPLINGS',          &
    UPDATE_INTERACTIONS  = 'UPDATE_INTERACTIONS',       &
    ITERATION_DONE       = 'ITERATION_DONE',            &
    REWIND_ALGORITHM     = 'REWIND_ALGORITHM'

  type(algorithmic_operation_t), public, parameter :: &
    OP_SKIP                = algorithmic_operation_t(SKIP,                'Skipping algorithmic operation'), &
    OP_UPDATE_COUPLINGS    = algorithmic_operation_t(UPDATE_COUPLINGS,    'Algorithmic operation - Updating couplings'), &
    OP_UPDATE_INTERACTIONS = algorithmic_operation_t(UPDATE_INTERACTIONS, 'Algorithmic operation - Updating interactions'), &
    OP_ITERATION_DONE      = algorithmic_operation_t(ITERATION_DONE,      'Iteration finished'), &
    OP_REWIND_ALGORITHM    = algorithmic_operation_t(REWIND_ALGORITHM,    'Rewind algorithm')
  !# doc_end


  !> Iterator to loop over the algorithmic operations of an algorithm
  type, extends(linked_list_iterator_t) :: algorithm_iterator_t
    private
  contains
    procedure :: get_next => algorithm_iterator_get_next
  end type algorithm_iterator_t


  !> @brief An algorithm is a list of algorithmic operations executed sequentially.
  !!
  !! This is implemented as a linked list of algorithmic operations.
  !
  type, extends(linked_list_t), abstract :: algorithm_t
    private
    type(algorithm_iterator_t), public :: iter            !< Iterator for algorithmic operations
    type(algorithmic_operation_t) :: current_ops          !< The current operation

    type(algorithmic_operation_t), public  :: start_operation !< @brief algorithm specific initialization operation;
    !!
    !!                                                           this operation is performed only once before the algorithm loop starts.

    type(algorithmic_operation_t), public  :: final_operation !< @brief algorithm specific finalization operation
    !!
    !!                                                           this operation is performed only once after the algorithm loop ends.

    integer, public :: algo_steps                         !< @brief Number of 'algorithmic steps' per algorithmic iteration
    !!
    !!                                                       This describes the granularity of the iteration. The algorithm iteration
    !!                                                       counter will advance algo_steps per iteration.
    !!
    !!                                                       This also means that the interactions have to be updated at
    !!                                                       algo_steps times per algorithm iteration.

    logical :: iteration_done                             !< Indicate whether the current iteration is done.

    type(iteration_counter_t), public :: iteration        !< Keep track at which iteration this algorithm is.
    FLOAT :: start_time = M_ZERO                          !< Keep the wall clock time when the algorithm started,
    FLOAT, public :: elapsed_time = M_ZERO                !< Elapsed wall clock time for printing info

  contains
    procedure :: add_operation => algorithm_add_operation                 !< @copydoc algorithm_add_operation
    procedure :: do_operation => algorithm_do_operation                   !< @copydoc algorithm_do_operation
    procedure :: update_elapsed_time => algorithm_update_elapsed_time     !< @copydoc algorithm_update_elapsed_time
    procedure :: rewind => algorithm_rewind                               !< @copydoc algorithm_rewind
    procedure :: next => algorithm_next                                   !< @copydoc algorithm_next
    procedure :: get_current_operation => algorithm_get_current_operation !< @copydoc algorithm_get_current_operation
    procedure(algorithm_finished), deferred :: finished                   !< @copydoc algorithm_finished
    procedure(algorithm_init_iteration_counters), deferred :: init_iteration_counters             !< @copydoc algorithm_init_iteration_counters
  end type algorithm_t

  abstract interface
    !> indicate whether the algorithm has finished one time step
    !
    logical function algorithm_finished(this)
      import :: algorithm_t
      class(algorithm_t), intent(in) :: this
    end function algorithm_finished

    !> initializes the algorithm and system iteration counters
    !
    subroutine algorithm_init_iteration_counters(this)
      import :: algorithm_t
      class(algorithm_t), intent(inout) :: this
    end subroutine algorithm_init_iteration_counters
  end interface

contains

  ! ---------------------------------------------------------
  !> add an algorithmic operation to the list
  !
  subroutine algorithm_add_operation(this, operation)
    class(algorithm_t),            intent(inout) :: this
    type(algorithmic_operation_t), intent(in)    :: operation

    PUSH_SUB(algorithm_add_operation)

    call this%add_copy(operation)

    POP_SUB(algorithm_add_operation)
  end subroutine algorithm_add_operation

  ! ---------------------------------------------------------
  !> try to perform one operation of the algorithm. If successfull return .true.
  !
  logical function algorithm_do_operation(this, operation) result(done)
    class(algorithm_t),            intent(inout) :: this
    type(algorithmic_operation_t), intent(in)    :: operation

    ! By default no algorithm specific operation is implemented in the algorithm
    ! class. Child classes that wish to change this behaviour should override
    ! this method.
    done = .false.

  end function algorithm_do_operation

  ! ---------------------------------------------------------
  !> The elapsed time is used for the output of run time information
  !
  subroutine algorithm_update_elapsed_time(this)
    class(algorithm_t), intent(inout) :: this

    PUSH_SUB(algorithm_update_elapsed_time)

    this%elapsed_time = loct_clock() - this%start_time

    POP_SUB(algorithm_update_elapsed_time)
  end subroutine algorithm_update_elapsed_time

  ! ---------------------------------------------------------
  !> @brief Reset the algorithm to the first operation
  !
  subroutine algorithm_rewind(this)
    class(algorithm_t), intent(inout) :: this

    PUSH_SUB(algorithm_rewind)

    ! Note that both lines are necessary to get the first element of the list.
    ! For more information, see the linked_list_t implementation.
    call this%iter%start(this)
    call this%next()
    this%start_time = loct_clock()

    POP_SUB(algorithm_rewind)
  end subroutine algorithm_rewind

  ! ---------------------------------------------------------
  !> move to the next algorithmic operation
  !
  subroutine algorithm_next(this)
    class(algorithm_t), intent(inout) :: this

    PUSH_SUB(algorithm_next)

    this%current_ops = this%iter%get_next()

    POP_SUB(algorithm_next)
  end subroutine algorithm_next

  ! ---------------------------------------------------------
  !> return the current algorithmic operation.
  !
  type(algorithmic_operation_t) function algorithm_get_current_operation(this) result(operation)
    class(algorithm_t), intent(in) :: this

    PUSH_SUB(algorithm_get_current_operation)

    operation = this%current_ops

    POP_SUB(algorithm_get_current_operation)
  end function algorithm_get_current_operation

  ! ---------------------------------------------------------
  !> @brief Get the next algorithmic operation from the iterator
  !!
  !! This function is necessary to specialize the deferred method of the linked list.
  !
  function algorithm_iterator_get_next(this) result(operation)
    class(algorithm_iterator_t),  intent(inout) :: this
    type(algorithmic_operation_t)               :: operation

    PUSH_SUB(algorithm_iterator_get_next)

    select type (ptr => this%get_next_ptr())
    class is (algorithmic_operation_t)
      operation = ptr
    class default
      ASSERT(.false.)
    end select

    POP_SUB(algorithm_iterator_get_next)
  end function algorithm_iterator_get_next

end module algorithm_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
