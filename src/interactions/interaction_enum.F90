!! Copyright (C) 2023 N. Tancogne-Dejean
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

!TODO:  this "enumerator" needs to be put back into the interactions factory once the lasers are out of the electrons
!! At the moment it is ouside due to circular dependencies
module interaction_enum_oct_m
  use debug_oct_m
  use global_oct_m
  implicit none

  private

  !# doc_start interaction_types
  integer, parameter, public :: &
    GRAVITY          = 1,       &
    LORENTZ_FORCE    = 2,       &
    COULOMB_FORCE    = 3,       &
    LINEAR_MEDIUM_TO_EM_FIELD = 4, &
    CURRENT_TO_MXLL_FIELD  = 5, &
    MXLL_E_FIELD_TO_MATTER   = 6,  &
    MXLL_B_FIELD_TO_MATTER   = 7,  &
    MXLL_VEC_POT_TO_MATTER   = 8,  &
    LENNARD_JONES            = 9
  !# doc_end

end module interaction_enum_oct_m

!! Local Variables:
!! mode: f90
!! coding: utf-8
!! End:
